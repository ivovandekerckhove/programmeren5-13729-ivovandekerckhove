// globals
var identity;
var procedureList;
var organisationList;
var position;
var myLocation;

/**
 * Tijdens het ontwikkelen van de app kan het nodig
 * zijn de localStorage leeg te maken
 */
var clearLocalStorage = function() {
    localStorage.removeItem('identity');
    localStorage.removeItem('procedureList');
    localStorage.removeItem('position');
    localStorage.removeItem('organisationList');
    localStorage.removeItem('personList');

}

/**
 * Vul het model in vanaf de JSON bestanden.
 *
 */
var loadData = function() {
    var callback = {
        identityLoaded: function(data) {
            if (!localStorage.getItem('identity')) {
                localStorage.setItem('identity', data);
            }
            identity = JSON.parse(localStorage.getItem('identity'));
        },
        procedureListLoaded: function(data) {
            if (!localStorage.getItem('procedureList')) {
                localStorage.setItem('procedureList', data);
            }
            procedureList = JSON.parse(localStorage.getItem('procedureList'));
        },
        positionLoaded: function(data) {
            if (!localStorage.getItem('position')) {
                localStorage.setItem('position', data);
            }
            position = JSON.parse(localStorage.getItem('position'));
        },
        organisationListLoaded: function(data) {
            if (!localStorage.getItem('organisationList')) {
                localStorage.setItem('organisationList', data);
            }
            organisationList = JSON.parse(localStorage.getItem('organisationList'));
        },
        personListLoaded: function(data) {
            if (!localStorage.getItem('personList')) {
                localStorage.setItem('personList', data);
            }
            personList = JSON.parse(localStorage.getItem('personList'));
        },
        error: function(data) {
            var p = document.createElement('P');
            p.innerHTML = '2 error: ' + data;
            p.style.color = '#ff0000';
            document.body.appendChild(p);
        }
    }

    $http('data/identity.json')
        .get()
        .then(function(data) {
            callback.identityLoaded(data);
            var payload = {};
            return $http('data/procedureList.json').get(payload);
        })
        .then(function(data) {
            callback.procedureListLoaded(data);
            var payload = {
                'id': 1
            };
            return $http('data/position.json').get(payload);
        })
        .then(function(data) {
            callback.positionLoaded(data);
            var payload = {};
            return $http('data/organisationList.json').get(payload);
        })
        .then(function(data) {
            callback.organisationListLoaded(data);
        })
        .then(function(data){
            callback.organisationListLoaded(data);
            var payload={};
            return $http('data/personList.json').get(payload);
        })
        .then(function(data){
            callback.personListLoaded(data);
        })
        .catch(callback.error);
};

/**
 * Dispath methode die de use case uitvoert die overeenkomt
 * met een de gevraagde interactie van de gebruiker.
 *
 * @param {object} e verwijzing naar het dom element dat het event heeft afgevuurd.
 */
var appDispatcher = function(e) {
    var target = e.target;
    if (target.tagName == 'SPAN') {
        target = target.parentNode;
    }
    if (target.getAttribute('name') == 'uc') {
        var uc = target.getAttribute('value');
        var path = uc.split('/');
        var entity = path[0] == undefined ? 'none' : path[0];
        var action = path[1] == undefined ? 'none' : path[1];
        var view = entity + '-' + action;
        // alert (entity + '/' + action);
        switch (entity) {
            case 'home':
                switch (action) {
                    case 'index':
                        navigateTo(view, 'veilig<br/><span>op school');
                        break;
                    case 'login':
                        navigateTo(view, 'veilig<br/><span>op school');
                        break;
                }
                break;
            case 'fire':
                switch (action) {
                    case 'index':
                        navigateTo(view, 'brand');
                        break;
                    case 'detection':
                        viewProcedure('BM');
                        navigateTo('view-procedure', 'brandmelding');
                        break;
                    case 'evacuation':
                        viewProcedure('BREV');
                        navigateTo('view-procedure', 'brand evacuatie');
                        break;
                }
                break;
            case 'page':
            case 'back':
                window.history.back();
                break;
            case 'poison':
                switch (action) {
                    case 'index':
                        navigateTo(view, 'Voedsel Vergiftiging');
                        break;
                    case 'oneperson':
                        viewProcedure('VVOP');
                        navigateTo('view-procedure', 'een persoon');
                        break;
                    case 'morepersons':
                        viewProcedure('VVMP');
                        navigateTo('view-procedure', 'meerdere personen');
                        break;
                }
                break;
            case 'bomb':
                break;
            case 'aidkit':
                break;
            case 'login':
                login();
                break;
        }
    }
}

/**
 * Ga naar de floor met de opgegeven view id en toon de tekst
 * in title in het h1 element.
 *
 * @param {string} view text id van de floor die getoond moet worden.
 * @param {string} title tekst die in het h1 element geplaatst moet worden.
 */
var navigateTo = function(view, title) {
    location.href = '#' + view;
    var h1 = document.querySelector('#' + view + ' h1');
    if (title && h1) {
        h1.innerHTML = title;
    }
}

/**
 * Haal de procedure op die door de gebruiker opgevraagd werd
 * en maak de view.
 *
 * @param {string} procedureCode tekst met de code van de op te vragen procedure.
 */
var viewProcedure = function(procedureCode) {
    var userRole = identity.role.toUpperCase();
    /**   var procedure = procedureList.list.find(function (item) {
           return item.code == procedureCode;
       });
       var role = procedure.role.find (function (item) {
           return item.code == userRole;
       });*/

    var procedure;
    for (var i = 0, len = procedureList.list.length; i < len; i++) {
        if (procedureList.list[i].code === procedureCode) {
            procedure = procedureList.list[i];
        }
    }
    var role;
    for (var i = 0, len = procedure.role.length; i < len; i++) {
        if (procedure.role[i].code === userRole) {
            role = procedure.role[i];
        }
    }
    elem = makeIdentity('#view-procedure .show-room');
    role.step.forEach(function(item, index) {
        var step = document.createElement('DIV');
        step.setAttribute('class', 'step');

        step.appendChild(makeTextElement(item.title, 'h2'));
        // acties
        var commandPanelElem = makeCommandPanel();
        item.action.forEach(function(item) {
            switch (item.code) {
                case 'TEL':
                    if (identity.loggedIn) {
                        commandPanelElem.appendChild(makeButton('Tel', 'icon-phone'));
                    }
                    else {
                        commandPanelElem.appendChild(makeTextElement(item.code + ' ' + item.phoneNumber, 'P'))
                    }
                    break;
                case 'SMS':
                    if (identity.loggedIn) {
//                        commandPanelElem.appendChild(makeButton('SMS', 'icon-send'));
						 var button = makeButton('SMS', 'icon-send');
                        button.setAttribute('onclick', 'smsPrepare("' + item.phoneNumber
                                            + '", "VOS incident")');
                        commandPanelElem.appendChild(button);
						
                    }
                    else {
                        commandPanelElem.appendChild(makeTextElement(item.code + ' ' + item.phoneNumber, 'P'))
                    }
                    break;
                case 'LIST':
                    var listElement = document.createElement('OL');
                    item.list.forEach(function(item) {
                        listElement.appendChild(makeTextElement(item.title, 'li'))
                    });
                    step.appendChild(listElement);
            }
            if (commandPanelElem.innerHTML != '') {
                step.appendChild(commandPanelElem);
            }
        });
        elem.appendChild(step);
    });
    // alert(JSON.stringify(role));
}

/**
 * Vul met identity model vanaf een JSON bestand.
 *
 * @param {string} querySelector de id van het HTML element waarin de gegevens van het model
 *      getoond moeten worden.
 */
var makeIdentity = function(querySelector) {
    var elem = document.querySelector(querySelector);
    elem.innerHTML = '';
    // alert(JSON.stringify(myLocation));
    if (identity.loggedIn) {
        elem.appendChild(makeTextElement(identity.firstName + ' ' + identity.lastName, 'h2'))
        elem.appendChild(makeTextElement(identity.function, 'h3'));
        elem.appendChild(makeTextElement(identity.mobile, 'h4'));
        elem.appendChild(makeTextElement(myLocation.name));
        elem.appendChild(makeTextElement(myLocation.street));
        elem.appendChild(makeTextElement(myLocation.phone));
		
    }
    else {
        elem.appendChild(makeTextElement(identity.userName, 'h2'))
         }
    return elem;
}

/**
 * Een html element creëren van het type opgegeven in de tag parameter. 
 * Plaats de tekst opgegeven in de text parameter in het gemaakte element.
 *
 * @param {string} text Text to be placed in the html element.
 * @param {string} tag type van het te maken html element.
 */
var makeTextElement = function(text, tag) {
    if (!tag) {
        tag = 'P';
    }
    var elem = document.createElement(tag);
    var text = document.createTextNode(text);
    elem.appendChild(text);
    return elem;
}

/**
 * Een button html element maken met een specifieke tekst erin en een bepaald icoon. 
 *
 * @param {string} text Text to be placed in the button element.
 * @param {string} icon klassenaam van het te tonen icoon.
 */
var makeButton = function(text, icon) {
    var buttonElem = document.createElement('BUTTON');
    buttonElem.setAttribute('type', 'submit');
    buttonElem.setAttribute('class', 'tile');
    var iconElem = document.createElement('SPAN');
    iconElem.setAttribute('class', icon);
    buttonElem.appendChild(iconElem);
    var screenReaderTextElem = document.createElement('SPAN');
    screenReaderTextElem.setAttribute('class', 'screen-reader-text');
    var textElem = document.createTextNode(text);
    screenReaderTextElem.appendChild(textElem);
    buttonElem.appendChild(screenReaderTextElem);
    return buttonElem;
}

/**
 * Een commandPanel element maken.
 *
 */
var makeCommandPanel = function() {
    var elem = document.createElement('DIV');
    elem.setAttribute('class', 'command-panel');
    return elem;
}

/**
 * Geolocatie van de telefoon ophalen
 *
 */
var getPosition = function() {

    var options = {
        maximumAge: 3600000,
        timeout: 6000,
        enableHighAccuracy: false
    }

    var onSuccess = function(pos) {

        position.latitude = pos.coords.latitude.toFixed(4);
        position.longitude = pos.coords.longitude.toFixed(4);
        position.altitude = pos.coords.altitude;
        position.accuracy = pos.coords.accuracy;
        position.aAltitudeAccuracy = pos.coords.altitudeAccuracy;
        position.heading = pos.coords.heading;
        position.speed = pos.coords.speed;
        position.timestamp = pos.coords.timestamp;

        getMyLocation();
        makeIdentity('#user-identity');
		
		if(callback){ 
			callback();
		}
    };

    var onError = function(error) {
        alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
        // stel in op hoofdzetel
        myLocation = organisationList[0];
        makeIdentity('#user-identity');
    };
    var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
}

/**
 * De dichtsbijzijnde organisatie ophalen.
 *
 */
var getMyLocation = function() {
    var i;
    for (i = 0; i < organisationList.length; i++) {
        if (organisationList[i].latitude >= position.latitude) {
            myLocation = organisationList[i];
            done = true;
            return;
        }
    }
    if (i = organisationList.length) {
        myLocation = organisationList[i - 1];
    }

}

/**
 * Voorbereiding versturen sms.
 *
 * @param {string} number telefoonnummer waarnaar sms gestuurd moet worden.
 */
var smsPrepare = function(number) {
    number = '0486788723';
    var message = 'Brandmelding\n' +
        identity.firstName + ' ' + identity.lastName + '\n' +
        myLocation.name + '\n' +
        myLocation.street + '\n' +
        myLocation.postalCode + ' ' + myLocation.city + '\n' +
        number;
    smsSend(number, message);
}

/**
 * De Cordova plugin gebruiken om een sms te versturen
 *
 * @param {string} number telefoonnummer waarnaar sms gestuurd moet worden.
 * @param {string} message de tekst van de te versturen sms.
 */
var smsSend = function(number, message) {
    // CONFIGURATION
    var options = {
        replaceLineBreaks: false, // true to replace \n by a new line, false by default
        android: {
            // intent: 'INTENT'  // send SMS with the native android SMS messaging
            intent: '' // send SMS without open any other app
        }
    };

    var success = function() {
        alert('Message sent successfully');
    };

    var error = function(e) {
        alert('Message Failed:' + e);
    };
    if (typeof sms === 'undefined' || typeof sms.send === 'undefined') {
        alert('SMS send is undefined. Would have sent error');
    }
    else {
        sms.send(number, message, options, success, error);
    }
}

var login = function() {
    var userName = document.getElementById('userName').value;
    var password = document.getElementById('userPassword').value;

    if (userName) {
        $http('data/personList.json')
        .get()
        .then(function(responseText) {
            var person = JSON.parse(responseText);
            var userIdentity;
            for(var i = 0, len = person.list.length;i<len;i++) {
                if(person.list[i].userName === userName && person.list[i].password === password) {
                    userIdentity = person.list[i];
					logoutVisible();
                }
            }
            if (userIdentity) {
                localStorage.removeItem('identity');
                localStorage.setItem('identity', JSON.stringify(userIdentity));
                // identity = JSON.parse(localStorage.getItem('identity'));
                identity = userIdentity;
                identity.loggedIn = true;
				getPosition(new function() { 
                navigateTo('home-index', 'veilig<br/><span>op school');
                });
                
            } else {
                alert('ongeldige gebruikersnaam of paswoord');
            }
        })
        .catch(function(){
            alert('Gebruikersdata kon niet geladen worden.');
        });
    } else {
        alert ('Je moet een gebruikersnaam opgeven!');
    }
}

/**
* controleer of er ingelogd werd
* laad het gastprofiel opnieuw in
* met dank aan Marc Wouters
*/

var logout = function() {
    if (identity) {
        localStorage.removeItem('identity');
        
        $http('data/identity.json')
        .get()
        .then(function(data) {
            if (!localStorage.getItem('identity')) {
                localStorage.setItem('identity', data);
            }
            identity = JSON.parse(localStorage.getItem('identity'));
            makeIdentity('#user-identity');
            navigateTo('home-index', 'veilig<br/><span>op school');
        });
    }
}


function loginVisible() {
    document.getElementById("logoutButton").style.display = "none";
}
function logoutVisible() {
    document.getElementById("loginButton").style.display = "none";
    document.getElementById("logoutButton").style.display = "block";
}