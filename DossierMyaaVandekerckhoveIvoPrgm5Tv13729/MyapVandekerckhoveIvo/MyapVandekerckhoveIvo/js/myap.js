﻿// Deze scripts heb ik deeld zelf gemaakt deels van het net van gehaald.

function FolderHide() {

    folderlabel.style.display = 'none';
    folderName.style.display = 'none';
    createFolderButton.style.display = 'none';
    renameFolderButton.style.display = 'none';
}


function createFolderVisible(clicked_id) {

    folderlabel.style.display = 'block';
    folderName.style.display = 'block';

    if (clicked_id == "createFolder") {
        createFolderButton.style.display = 'block';
        renameFolderButton.style.display = 'none';
    }


    else {

        createFolderButton.style.display = 'none';
        renameFolderButton.style.display = 'none';

    }
}

function clearExplorer() {
    var list = document.querySelector('#list');
    list.innerHTML = '';
    return list;
}


function clearEditor() {

    location.reload(true);

}

function clearAll() {
    document.getElementById('text-editor').style.display = 'none';
    document.getElementById('testVerkenner').style.display = 'none';
    document.getElementById('testFolder').style.display = 'none';
    document.getElementById('testExplorer').style.display = 'none';

}

function templateEditor() {



    document.getElementById('editor').innerHTML = '';

}


function renameFolderVisible(id, name) {

    createFolderButton.style.display = 'none';
    folderlabel.style.display = 'block';
    folderName.style.display = 'block';
    renameFolderButton.style.display = 'block';

    document.getElementById('renameFolderId').value = id;
    document.getElementById('folderName').value = name;

}


function prepareRenameFolder() {

    var id = document.getElementById('renameFolderId').value;
    var newName = document.getElementById('folderName').value;

    renameFile(id, newName);


}


function renameFile(id, newName) {

    var data = { 'name': newName, 'fileId': id };
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.patchRequest(url,
        JSON.stringify(data),
        function (responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);

}

function useTemplate() {

    document.getElementById('editorvisible').style.display = 'none';
    document.getElementById('selecteditor').value = 'test';
    var template = document.querySelector('#template');
    var clone = document.importNode(template.content, true);
    var host = document.querySelector('#host');
    host.appendChild(clone);
}


function prepareUpdateEditor() {

    var text = document.getElementById('editor').value;
    var fileId = document.getElementById('idTextEditor').value;
    var folderId = document.getElementById('currentFolderId').value;


    if (text.length > 0) {

        updateEditor(fileId, folderId, text);
    }
    else {

        alert('Geen data beschikbaar.');
    }

}



function updateEditor(fileId, folderId, text, callback) {

    const boundary = '-------314159265358979323846';
    const delimiter = "\r\n--" + boundary + "\r\n";
    const close_delim = "\r\n--" + boundary + "--";

    var contentType = "text/html";
    var metadata = { 'mimeType': contentType, };
    var multipartRequestBody =
        delimiter + 'Content-Type: application/json\r\n\r\n' +
        JSON.stringify(metadata) +
        delimiter + 'Content-Type: ' + contentType + '\r\n' + '\r\n' +
        text +
        close_delim;
    if (!callback) {
        callback = function (file) {
            // console.log("Update Complete ", file)
        };
    }
    gapi.client.request({
        'path': '/upload/drive/v2/files/' + folderId + "?fileId=" + fileId + "&uploadType=multipart",
        'method': 'PUT',
        'params': { 'fileId': fileId, 'uploadType': 'multipart' },
        'headers': { 'Content-Type': 'multipart/mixed; boundary="' + boundary + '"' },
        'body': multipartRequestBody,
        callback: callback,
    });



    location.reload(true);
}


function autoSaveEditor() {

    intervalId = setInterval(function () { prepareUpdateEditor() }, 10000);

}

function stopAutoSaveEditor() {

    clearInterval(intervalId);

}







