// variabelen
var timerId1 = 0;
var timerId2 = 2;
var heli1;
var heli1Right;
var heli1Right2;
var car;
var carRight;


var startAnimation = function() {

    // opstarten van de twee verschillende animaties die lopen
    heli1 = document.getElementById('heli1');
    heli1Right = 0;
    car = document.getElementById('car');
    carRight = 0;

    // de twee verschillende timer events
    timerId1 = window.setInterval(moveHeli, 10);

    timerId2 = window.setInterval(carMove, 15);

}

var startHeli2 = function() {
    // om de helicopter een tweede maal te doen starten

    heli1 = document.getElementById('heli1');
    heli1Right = 500;
    timerId1 = window.setInterval(moveHeli2, 10);

}

var moveHeli = function() {
    // start van de helicopter

    heli1Right += 2;
    heli1.style.right = heli1Right + 'px';

    // de animatie stoppen

    if (heli1Right == 500) {
        holdAnimation();
    }
}

var moveHeli2 = function() {

    // de helicopter terug starten

    heli1Right += 2;
    heli1.style.right = heli1Right + 'px';

}

var carMove = function() {

    // de wagen laten rijden

    carRight += 2;

    car.style.right = carRight + 'px';

}

var holdAnimation = function() {

    // de helicoter stoppen

    clearInterval(timerId1);

    window.setTimeout(startHeli2, 2000);

}
