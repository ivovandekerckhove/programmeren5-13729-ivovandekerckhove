/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.

function opstart(){
  
    document.getElementById('textH1').style.display='block';
    document.getElementById('textH2').style.display='none';
    document.getElementById('text1').style.display='block';
    document.getElementById('text2').style.display='none';
    document.getElementById('reset').style.display='none';
    document.getElementById('id_btnHello').style.display='block';
}




function myEventHandler() {
     
    newTitel();
}




function newTitel(){
    
    document.getElementById('textH1').style.display='none';
    document.getElementById('textH2').style.display='block';
    document.getElementById('text1').style.display='none';
    document.getElementById('text2').style.display='block';
    document.getElementById('reset').style.display='block';
    document.getElementById('id_btnHello').style.display='none';
    document.body.style.backgroundColor = "#db3333";
}


function reset(){
    
    location.reload(true);
}